﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneVideo : MonoBehaviour
{
  public void BackMainMenu()
    {
        Application.LoadLevel("MainMenu");
    }

    public void GoMateri()
    {
        Application.LoadLevel("Materi");
    }

    public void GoVideo1()
    {
        Application.LoadLevel("Video1");
    }

    public void GoVideo2()
    {
        Application.LoadLevel("Video2");
    }

    public void GoVideo3()
    {
        Application.LoadLevel("Video3");
    }

    public void GoVideo4()
    {
        Application.LoadLevel("Video4");
    }

    public void GoVideo5()
    {
        Application.LoadLevel("Video5");
    }

    public void GoVideo6()
    {
        Application.LoadLevel("Video6");
    }
}
