﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class buttonaction : MonoBehaviour
{
    public AudioClip sound;
    private Button button { get { return GetComponent<Button>();}}
    private AudioSource source { get { return GetComponent<AudioSource>(); } }

    private void Start()
    {
        gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;

        button.onClick.AddListener(() => PlaySound());

    }

    void PlaySound()
    {
        source.PlayOneShot(sound);
    }

}
