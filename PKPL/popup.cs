﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class popup : MonoBehaviour
{
    public GameObject Popup;

    // Start is called before the first frame update
    public void open()
    {
        if (Popup != null)
        {
            Popup.SetActive(true);
        }
    }
}
