﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class skor : MonoBehaviour
{
    public static int coin;
    Text score;

    // Use this for initialization
    void Start()
    {
        coin = 0;
        score = GetComponent<Text>();
    }
    void Update()
    {
        score.text = coin.ToString();
    }
}
